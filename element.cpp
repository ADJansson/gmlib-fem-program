#include "element.h"

Element::Element() {

}


Element::Element(GMlib::TSEdge<float> *edge, GMlib::TSVertex<float> *p0, GMlib::TSVertex<float> *p1, int i, int j) {

    _edge = edge;
    _p0 = p0;
    _p1 = p1;
    _i = i;
    _j = j;

}


Element::~Element() {

}


/**
 * @brief Element::getValue
 * @return Stiffness matrix value
 */
float Element::getValue() {

    return _i == _j ? calculateDiagonal() : calculateNonDiagonal();
}


/**
 * @brief Element::calculateNotDiagonal
 * @return Stiffness matrix value for non-diagonal element
 */
float Element::calculateNonDiagonal() {

    GMlib::TSVertex<float> *p2, *p3;

    GMlib::Vector<float, 2> d, a1, a2;

    auto triangle1 = _edge->getTriangle()[1];
    auto triangle2 = _edge->getTriangle()[0];

    for (int i = 0; i < triangle1->getVertices().size(); i++) {
        if (triangle1->getVertices()[i] != _p0 && triangle1->getVertices()[i] != _p1)
            p2 = triangle1->getVertices()[i];
    }

    for (int i = 0; i < triangle2->getVertices().size(); i++) {
        if (triangle2->getVertices()[i] != _p0 && triangle2->getVertices()[i] != _p1)
            p3 = triangle2->getVertices()[i];
    }

    d = _p1->getPosition() - _p0->getPosition();
    a1 = p2->getPosition() - _p0->getPosition();
    a2 = p3->getPosition() - _p0->getPosition();

    // Formulae

    float dd = 1 / (d * d);

    // Triangle (T1)

    float area1 = std::abs(d ^ a1);
    float dh1 = dd * (a1 * d);
    float h1 = dd * area1 * area1;

    // Triangle (T2)

    float area2 = std::abs(a2 ^ d);
    float dh2 = dd * (a2 * d);
    float h2 = dd * area2 * area2;

    return ((dh1 * (1 - dh1) / h1) - dd) * (area1 / 2)
            + ((dh2 * (1 - dh2) / h2) - dd) * (area2 / 2);

} // END calculateNonDiagonal


/**
 * @brief Element::calculateDiagonal
 * Calulates diagonal value, in addition to load vector value
 * @return Stiffness matrix value for diagonal element
 */
float Element::calculateDiagonal() {

    float sumDiagonal = 0;
    _load = 0;

    auto triangles = _p0->getTriangles();

    for (int i = 0; i < triangles.size(); i++) {

        auto vertices = triangles[i]->getVertices();

        // Check, and if necessary, change orientation in accordance with formulae
        if (_p0 == vertices[1]) {

            std::swap(vertices[0], vertices[1]);
            std::swap(vertices[1], vertices[2]);
        }

        if (_p0 == vertices[2]) {
            std::swap(vertices[0], vertices[2]);
            std::swap(vertices[1], vertices[2]);
        }

        GMlib::TSVertex<float>* p1 = vertices[1];
        GMlib::TSVertex<float>* p2 = vertices[2];

        GMlib::Vector<float, 2> d1 = p2->getPosition() - _p0->getPosition();
        GMlib::Vector<float, 2> d2 = p1->getPosition() - _p0->getPosition();
        GMlib::Vector<float, 2> d3 = p2->getPosition() - p1->getPosition();

        sumDiagonal += (d3 * d3) / (2 * std::abs(d1 ^ d2));

        // Calulate load vector value also
        _load += std::abs(d1 ^ d2) / 6;
    }

    return sumDiagonal;

} // END calculateDiagonal


float Element::getLoad() {
    return _load;
}


int Element::getI() {
    return _i;
}


int Element::getJ() {
    return _j;
}

