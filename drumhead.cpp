#include "drumhead.h"
#include <chrono>

Drumhead::Drumhead(GMlib::ArrayLX<GMlib::TSVertex<float>> pointset, float radius = 15)
    : GMlib::TriangleFacets<float>(pointset) {
    _radius = radius;
}


Drumhead::~Drumhead() {
    _elements.clear();
    _pointset_internal.clear();
}


/**
 * @brief Drumhead::init
 * Initiates a new drum
 */
void Drumhead::init() {

    this->triangulateDelaunay();
    removeBoundary();
    generateElements();
    initMatrix();
    computeStiffnessMat();
    this->computeNormals();
    this->replot();

}


/**
 * @brief Drumhead::removeBoundary
 * Adds all internal vertices to separate array
 */
void Drumhead::removeBoundary() {
    for (int i = 0; i < this->size(); i++) {
        if (!this->getVertex(i)->boundary())
            _pointset_internal += this->getVertex(i);
    }

}


/**
 * @brief Drumhead::generateElements
 * Loops through all internal vertices and creates FEM elements where triangles overlap
 */
void Drumhead::generateElements() {

    for (int i = 0; i < _pointset_internal.size(); i++) {

        auto p0 = _pointset_internal[i];

        for (int j = i; j < _pointset_internal.size(); j++) {

            auto p1 = _pointset_internal[j];

            GMlib::TSEdge<float>* commonEdge = getCommonEdge(p0, p1);

            if (commonEdge != nullptr || i == j) {
                _elements += new Element(commonEdge, _pointset_internal[i], _pointset_internal[j], i, j);
            }

        }
    }

} // END generateElements


/**
 * @brief Drumhead::getCommonEdge
 * @param p0 The first vertex
 * @param p1 The other vertex
 * @return A common edge if found, else nullptr
 */
GMlib::TSEdge<float>* Drumhead::getCommonEdge(GMlib::TSVertex<float> *p0, GMlib::TSVertex<float> *p1) {

    auto edges = p0->getEdges();

    for (int i = 0; i < edges.size(); i++) {

        if (edges[i]->getOtherVertex(*p0) == p1) {
            return edges[i];
        }
    }
    return nullptr;
}


/**
 * @brief Drumhead::initMatrix
 * Sets dimensions and fills matrix with 0's
 */
void Drumhead::initMatrix() {

    _sMat.setDim(_pointset_internal.size(), _pointset_internal.size());
    _fVec.setDim(_pointset_internal.size());

    for (int i = 0; i < _sMat.getDim1(); i++) {
        for (int j = 0; j < _sMat.getDim2(); j++) {
            _sMat[i][j] = 0;
        }
    }
}


/**
 * @brief Drumhead::computeStiffnessMat
 * Computes the stiffness matrix and load vector from generated elements
 */
void Drumhead::computeStiffnessMat() {

    std::chrono::time_point<std::chrono::system_clock> start, end;

    start = std::chrono::system_clock::now();

    for (int k = 0; k < _elements.size(); k++) {
        auto element = _elements[k];

        int i = element->getI();
        int j = element->getJ();

        _sMat[i][j] = _sMat[j][i] = element->getValue();

        // Also load vector value
        if (i == j)
            _fVec[i] = element->getLoad();
    }

    _sMat_inverted = _sMat.invert();

    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end - start;

    std::cout<<"Computed "<< _elements.size()<<" FEM elements in "<< elapsed_seconds.count()<<" seconds."<<std::endl;

} // END computeStiffnessMat


/**
 * @brief Drumhead::localSimulate
 * Computes x-vector and replots simulation
 * @param dt Elapsed time
 */
void Drumhead::localSimulate(double dt) {

    float scaler = 3.f / _radius;

    _simsim = _simsim + (_increaseDt ? (dt * scaler) : (-dt * scaler));
    if (_simsim >= scaler || _simsim <= -scaler)
        _increaseDt = !_increaseDt;

    auto x =_sMat_inverted * (_fVec * _simsim);

    for (int i = 0; i < _pointset_internal.size(); i++) {
        _pointset_internal[i]->setZ(x[i]);
    }

    this->replot();

}
