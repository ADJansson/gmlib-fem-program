#ifndef ELEMENT_H
#define ELEMENT_H
#include <core/gmarraylx>
#include <trianglesystem/gmtrianglesystem>

class Element {

private:
    GMlib::TSEdge<float>  *_edge;

    float calculateNonDiagonal();
    float calculateDiagonal();

    GMlib::TSVertex<float> *_p0, *_p1;
    int _i, _j;
    float _load;

public:
    Element();
    Element(GMlib::TSEdge<float> *edge, GMlib::TSVertex<float> *p0, GMlib::TSVertex<float> *p1, int i, int j);
    ~Element();

    float getValue();
    float getLoad();
    int getI();
    int getJ();
};

#endif // ELEMENT_H

