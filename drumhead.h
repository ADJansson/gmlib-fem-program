#ifndef DRUMHEAD_H
#define DRUMHEAD_H

#include "element.h"

#include <core/gmarraylx>
#include <trianglesystem/gmtrianglesystem>

class Drumhead : public GMlib::TriangleFacets<float> {
private:
    GMlib::ArrayLX<GMlib::TSVertex<float>*> _pointset_internal;
    GMlib::ArrayLX<Element*> _elements;
    GMlib::DMatrix<float> _sMat, _sMat_inverted;
    GMlib::DVector<float> _fVec;
    float _simsim;
    bool _increaseDt;
    float _radius;

    void removeBoundary();
    void generateElements();
    GMlib::TSEdge<float>* getCommonEdge(GMlib::TSVertex<float> *p0, GMlib::TSVertex<float> *p1);
    void initMatrix();
    void computeStiffnessMat();

public:
    Drumhead(GMlib::ArrayLX<GMlib::TSVertex<float>> pointset, float radius);
    ~Drumhead();
    void init();

protected:
    void localSimulate(double dt);
};

#endif // DRUMHEAD_H

